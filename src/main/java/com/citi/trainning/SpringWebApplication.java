package com.citi.trainning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebApplication {
//entry point to the application, starts up tomcat server
	public static void main(String[] args) {
		SpringApplication.run(SpringWebApplication.class, args);
	}
}
