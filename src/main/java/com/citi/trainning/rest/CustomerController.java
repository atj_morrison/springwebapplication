package com.citi.trainning.rest;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.trainning.business.CustomerBs;
import com.citi.trainning.dto.Customer;

@RestController
@RequestMapping("/customers")
// /customers=entry point for the url
//http://localhost:8082/customers/
public class CustomerController {
	
	private static final Logger logger = LogManager.getLogger (CustomerController.class);
	
	@Autowired
	CustomerBs customerBs;
	
	//add get status method to check there is access
	@RequestMapping(value="/status", method= RequestMethod.GET)
	public String getStatus() {
		logger.info("customer controller started");
		return "Customer Controller running";
	}
	
	@RequestMapping(value="/list", method= RequestMethod.GET)
	public List<Customer> findAll() 
	{
		return customerBs.findAll();
	}
	//
	//http://localhost:8082/customers/findbyid?id=anton
	@RequestMapping(value="/findbyid", method= RequestMethod.GET)
	public Customer findbyId(String id)
	{
		return customerBs.findbyId(id);
	}
	@RequestMapping(value="/save", method= RequestMethod.POST)
	public int save(@RequestBody Customer customer)
	{
		return customerBs.save(customer);
		
	}
	
	
	//update has to be passed in in the request body
	@RequestMapping(value="/update", method= RequestMethod.PUT)
	public Customer update(@RequestBody Customer customer) 
	{
		return customerBs.update(customer);
	}
	

	

}
