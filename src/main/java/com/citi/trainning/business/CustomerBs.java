package com.citi.trainning.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.trainning.dao.CustomerDao;
import com.citi.trainning.dto.Customer;

@Component
public class CustomerBs {
	
	@Autowired
	CustomerDao customerDao;
	
	public int rowCount() {
		return customerDao.rowCount();
	}
	
	public Customer update(Customer customer) {
		if (customer.getName().equals(""))
		{
			throw new IllegalArgumentException();
		}
		return customerDao.update(customer);
	}
	
	public Customer findbyId (String id) {
		if (id.length()!=5)
		{
			throw new IllegalArgumentException();
		}
		return customerDao.findbyId(id);
	}
	public List<Customer> findAll() {
		return customerDao.findAll();
	}
	
	public int save(Customer customer) {
		if(customer.getName()==null) {
			throw new IllegalArgumentException();
		}
		
		return customerDao.save(customer);
	}
}
