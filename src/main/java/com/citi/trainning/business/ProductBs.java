package com.citi.trainning.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.citi.trainning.dao.ProductDao;
import com.citi.trainning.dto.Product;


@Component
public class ProductBs {

	@Autowired
	ProductDao productDao;
	Product product;
	
	public int rowCount() {
		return productDao.rowCount();
	}
	
	//TODO check this method is right, needs tested
	public Product findById(int ProductId) {
		
		if(ProductId >0) {
			throw new IllegalArgumentException();
		}
		else {
			productDao.findbyId(ProductId);
		}
		return product;
	}
	
	
}
