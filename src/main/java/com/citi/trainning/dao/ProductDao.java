package com.citi.trainning.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.citi.trainning.dto.Product;



@Component
public class ProductDao {
	@Autowired
	private JdbcTemplate tpl;
	
	public int rowCount() 
	{
		return tpl.queryForObject("select count(*) from products", 
				Integer.class);
	}
	
//TODO change to product details	
	public Product findbyId(int id) 
	{
		
		return tpl.queryForObject("select * from products where ProductID=1", 
				new ProductMapper());
	}
	
	
	private static final class ProductMapper 
	implements RowMapper<Product> {
	    public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
	        Product product = new Product
	        		(rs.getInt("ProductId"),
	        		 rs.getString("ProductName"),
	        		 rs.getInt("SupplierId"),
	        		 rs.getInt("CategoryId"),
	        		 rs.getString("QuantityPerUnit"),
	        		 rs.getDouble("UnitPrice"),
	        		 rs.getInt("UnitsInStock"),
	        		 rs.getInt("ReorderLevel"),
	        		 rs.getInt("Discontinued"));	        		 		
	        		 
	        		 return product;
	    }

	
	

}
}
