package com.citi.trainning.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.trainning.dto.Customer;
import com.citi.trainning.rest.CustomerController;
import com.mysql.jdbc.Statement;

//first and foremost all SPRING BEANS are managed and they live inside
//a container called an APPLICATION CONTEXT
//@Component makes it a bean and spring instantiates the class automatically 
//when it is used no need for =new


@Component
public class CustomerDao {
	private static final Logger logger = LogManager.getLogger (CustomerController.class);
	
	//autowired tells the application context to inject an instance if the jdbc templatehere
	
	@Autowired
	private JdbcTemplate tpl;
	
	public int rowCount() 
	{
		return tpl.queryForObject("Select count(*) from customers", 
				Integer.class);
	}

	public Customer findbyId(String id) 
	{
		
		return tpl.queryForObject("select * from customers where CustomerID=?",
				new Object [] {id},
				new CustomerMapper());
	}
	public List<Customer> findAll(){
		logger.info("Find All invoked");
		return tpl.query("Select * from customers",
				new CustomerMapper());
	}
	
//insert into customers(CustomerID,CompanyName,ContactName,Address,City,Country)
//values ('A1','Avohn','Aoibheann','Belfast','Antrim','Ireland') 
	
	
	//insert statements
	public int save (Customer customer)
	{
		KeyHolder keyHolder = new GeneratedKeyHolder();
		this.tpl.update(
				new PreparedStatementCreator() {
					@Override
					public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
						PreparedStatement ps =
								connection.prepareStatement("insert into customers(CustomerID,CompanyName,ContactName,Address,City,Country) "
											+ " values (?, ?, ?, ?, ?, ?)",
								Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, customer.getCustomerId());
		ps.setString(2, customer.getName());
		ps.setString(3, customer.getContactname());
		ps.setString(4, customer.getAddress());
		ps.setString(5, customer.getCity());
		ps.setString(6, customer.getCountry());
		return ps;
					}
				},
				keyHolder);
				//return keyHolder.getKey().intValue();
				return 0;
	}	
	
	//Update database
	public Customer update(Customer customer)
	{
		tpl.update("Update customers set CompanyName=?, ContactName=?, Address=?, City=?, Country=? Where CustomerID=?",
				customer.getName(),
				customer.getContactname(),
				customer.getAddress(),
				customer.getCity(),
				customer.getCountry(),
				customer.getCustomerId()
				);
		return customer;
	}
	
	private static final class CustomerMapper 
	implements RowMapper<Customer> {
	    public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
	        Customer customer = new Customer
	        		(rs.getString("customerid"),
	        		 rs.getString("companyname"),
	        		 rs.getString("contactname"),
	        		 rs.getString("address"),
	        		 rs.getString("city"),
	        		 rs.getString("country"));
	        		 return customer;
	    }
	    
}
}
