package com.citi.trainning.dto;

public class Product extends DbMetadata {

	private int ProductId;
	private String ProductName;
	private int SupplierId;
	private int CategoryId;
	private String QuantityPerUnit;
	private double UnitPrice;
	private int UnitsInStock;
	private int ReorderLevel;
	private int Discontinued;
	
	
	public Product() {}
	public Product(int productId, String productName, int supplierId, int categoryId, String quantityPerUnit,
			double unitPrice, int unitsInStock, int reorderLevel, int discontinued) {
		
		ProductId = productId;
		ProductName = productName;
		SupplierId = supplierId;
		CategoryId = categoryId;
		QuantityPerUnit = quantityPerUnit;
		UnitPrice = unitPrice;
		UnitsInStock = unitsInStock;
		ReorderLevel = reorderLevel;
		Discontinued = discontinued;
	}

	public int getProductId() {
		return ProductId;
	}

	public void setProductId(int productId) {
		ProductId = productId;
	}

	public String getProductName() {
		return ProductName;
	}

	public void setProductName(String productName) {
		ProductName = productName;
	}

	public int getSupplierId() {
		return SupplierId;
	}

	public void setSupplierId(int supplierId) {
		SupplierId = supplierId;
	}

	public int getCategoryId() {
		return CategoryId;
	}

	public void setCategoryId(int categoryId) {
		CategoryId = categoryId;
	}

	public String getQuantityPerUnit() {
		return QuantityPerUnit;
	}

	public void setQuantityPerUnit(String quantityPerUnit) {
		QuantityPerUnit = quantityPerUnit;
	}

	public double getUnitPrice() {
		return UnitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		UnitPrice = unitPrice;
	}

	public int getUnitsInStock() {
		return UnitsInStock;
	}

	public void setUnitsInStock(int unitsInStock) {
		UnitsInStock = unitsInStock;
	}

	public int getReorderLevel() {
		return ReorderLevel;
	}

	public void setReorderLevel(int reorderLevel) {
		ReorderLevel = reorderLevel;
	}

	public int getDiscontinued() {
		return Discontinued;
	}

	public void setDiscontinued(int discontinued) {
		Discontinued = discontinued;
	}

	
	
}