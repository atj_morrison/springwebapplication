package com.citi.trainning.dto;

public class Customer extends DbMetadata {
	
	private String customerId;
	private String name;
	private String contactname;
	private String address;
	private String city;
	private String country;
	
	public Customer () {}

	public Customer(String customer, 
						String name, String contactname, String address, 
							String city, String country) {
		this.customerId = customer;
		this.name = name;
		this.contactname = contactname;
		this.address = address;
		this.city = city;
		this.country = country;
	}


	public String getCustomerId() {
		return customerId;
	}


	public void setCustomerId(String customer) {
		this.customerId = customer;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getContactname() {
		return contactname;
	}


	public void setContactname(String contactname) {
		this.contactname = contactname;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}
	
	
	
	
}
