package com.citi.trainning.business;


import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trainning.dto.Customer;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerBsIT {

	@Autowired
	CustomerBs customerBs;
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test (expected=IllegalArgumentException.class)
	public void CustomerBs_InvalidIdLength_ReturnsIllegalArgumentException() {
		customerBs.findbyId("Anto");
	}
	
	@Test ()
	public void CustomerBs_ValidIdLength_ReturnsCustomer() {
		Customer customer = customerBs.findbyId("Anton");
		System.out.println("---> " + customer.getContactname());
		assertThat(customer, is(not(nullValue())));
		//assert(!customer.equals(null));
	}
	
	@Test ()
	public void CustomerBs_ListAllCustomers_ReturnsListOfCustomer() {
		List<Customer> customer=customerBs.findAll();
		assertTrue(customer.size()==customerBs.rowCount());
	}
	
	@Test
	public void customerBs_SaveCustomer_ReturnsCustomer() {
		int result=customerBs.save(new Customer("a001","xltd", "Ave",
				"belfast", "Antrim","NI"));
		Customer customer = customerBs.findbyId("a001");
		assertTrue(customer.getContactname().equals("Ave"));
		
		//delete after
	}
	
	
	
	

}
