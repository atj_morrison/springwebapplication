package com.citi.trainning.Dao;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trainning.dao.CustomerDao;
import com.citi.trainning.dto.Customer;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerDaoIT {

	@Autowired
	CustomerDao customerDao;
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void customerDao_CheckRowCount_ReturnsOver0() {
		assertTrue (customerDao.rowCount()>0);
	}
	
	@Test
	public void customerDao_CheckFindById_ReturnsContact()
	//TODO make more general, this name could be deleted from it
	{
		Customer customer =customerDao.findbyId("Anton");
		assertTrue (customer.getContactname().equals("Antonio Moreno"));
	}
	
	@Test 
	public void customerDao_CheckFindAll_ReturnsAllRecords() {
		List<Customer> customers=customerDao.findAll();
		
		assertTrue (customers.size()==
			customerDao.rowCount());}
	
	@Test
	public void customerDao_SaveCustomer_ReturnsCustomer() {
		int result=customerDao.save(new Customer("b001","xltd", "bob",
				"belfast", "Antrim","NI"));
		Customer customer = customerDao.findbyId("b001");
		assertTrue(customer.getContactname().equals("bob"));
		
		//delete after
	}
	
	@Test
	public void customerDao_UpdateCustomer_ReturnsCustomer() {
		customerDao.update(new Customer("BERGS","xltd", "Bob",
				"main St", "Belfast","NI"));
		Customer customer = customerDao.findbyId("BERGS");
		assertTrue(customer.getContactname().equals("Bob"));
		assertTrue(customer.getCity().equals("Belfast"));
		
	}
	

}
