package com.citi.trainning.Dao;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trainning.dao.ProductDao;
import com.citi.trainning.dto.Product;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductDaoIT {
	@Autowired
	ProductDao productDao;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void productDao_CheckRowCount_ReturnsOver0() {
		System.out.println(productDao);
		assertTrue (productDao.rowCount()>0);
	}
	
	

	@Test
	public void productDao_CheckFindById_ReturnsProductName()
	//TODO make more general, this name could be deleted from it
	{
		Product product =productDao.findbyId(1);
		assertTrue (product.getProductName().equals("Chai"));
	}
}
