package com.citi.trainning.dto;


import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerTests {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void Customer_CanInstantiate_ReturnsNotNull() {
		
		assertThat (new Customer ("ORCL", "Oracle", "Deidre","Main St", "Belfast",
				"NI"), is(not(nullValue())));
	}

}
